# Upgrading from 5.7 to the new SDK

In order to use our latest SDK features, follow the steps below.

## 1 - Get rid of deprecated features

Start by removing references of DefaultUI and Voice, since they don't exist anymore.
<br>
*ETA*: 30 minutes depending on how many features you are using from our SDK.

## 2 - Remove the singletons references

Run the *Clean Imports* command of Android Studio, since the classes Visitor, PresenceVisitor, and GlanceManager are no longer public. Instead, use the new Glance class to access the SDK features. Remove them first to make it easier to find the places to change (by following the compilation errors).
<br>
*ETA*: 5 minutes.

## 3 - Check your Manifest file

If you are using the ```allowBackup``` property, make sure you set it to ```false```.
<br>
*ETA*: 1 minute.

## 4 - Remove any old XML resource reference

Do not reuse any Glance SDK icons, colors, fonts, etc since this new coreSDK doesn't have any UI elements. If you are having issues with that, review the resources documentation [here](https://help.glance.net/mobile-app-sharing/getting-started/android/android_default_ui/).
<br>
*ETA*: 30 minutes depending on how many UI resources you are reusing from our SDK.

## 4 - Follow the migration table

| What                               | ETA   | 5.X                                                                                                                                                   | New SDK                                                                                                                                                    | 
|------------------------------------|-------|-------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Initialization                     | 1 min | ```Visitor.init(Activity activity, VisitorInitParams initParams)```                                                                                   | ```Glance.init(Activity activity, VisitorInitParams initParams)```                                                                                         |
|                                    | 1 min | ```Visitor.init(Activity activity, int groupid, String token, String name, String email, String phone, String visitorId, VisitorListener listener)``` | ```Glance.init(Activity activity, int groupId, String token, String name, String email, String phone, String visitorId, VisitorListener eventsListener)``` |
| Starting a session                 | 1 min | ```Visitor.startSession()```                                                                                                                          | ```Glance.startSession(String key)```                                                                                                                      |
|                                    | 5 min | ```Visitor.startSession()```                                                                                                                          | ```Glance.startSession(StartParams p)```                                                                                                                   |
|                                    | 5 min | ```Visitor.startSession()```                                                                                                                          | ```Glance.startSession(StartParams p, boolean skipDialog)```                                                                                               |
| Enabling/Disabling UI              | 1 min | ```Visitor.defaultUI(...)```                                                                                                                          | ```- (not needed anymore)```                                                                                                                               |
| Setting the Events listener        | 1 min | ```Visitor.addListener(VisitorListener listener)```                                                                                                   | ```Glance.addVisitorListener(VisitorListener listener)```                                                                                                  |
| Removing the Events listener       | 1 min | ```Visitor.removeListener(VisitorListener listener)```                                                                                                | ```Glance.removeVisitorListener(VisitorListener listener)```                                                                                               |
| Adding masked views                | 1 min | ```Visitor.addMaskedViewId(int viewId, final String label)```                                                                                         | ```Glance.addMaskedViewId(int viewId, final String label) ```                                                                                              |
|                                    | 1 min | ```Visitor.addMaskedView(final View view, final String label)```                                                                                      | ```Glance.addMaskedView(final View view, final String label)```                                                                                            |
| Removing masked views              | 1 min | ```Visitor.removeMaskedViewId(int viewId)```                                                                                                          | ```Glance.removeMaskedViewId(int viewId)```                                                                                                                |
|                                    | 1 min | ```Visitor.removeMaskedView(final View view)```                                                                                                       | ```Glance.removeMaskedView(final View view)```                                                                                                             |
| Ending a session                   | 1 min | ```Visitor.endSession()```                                                                                                                            | ```Glance.endSession()```                                                                                                                                  |
| Connecting to Presence             | 1 min | ```PresenceVisitor.connect()```                                                                                                                       | ```Glance.connectToPresence()```                                                                                                                           |
| Enabling/Disabling UI for Presence | 1 min | ```PresenceVisitor.setDefaultUI(boolean enabled)```                                                                                                   | ```- (not needed anymore)```                                                                                                                               |
| Sending a message through Presence | 1 min | ```PresenceVisitor.presence(Map paramsMap)```                                                                                                         | ```Glance.sendToPresenceSession(String event, Map<String, String> map)```                                                                                  |
| Disconnecting from Presence        | 1 min | ```PresenceVisitor.disconnect()```                                                                                                                    | ```Glance.disconnectPresence()```                                                                                                                          |
| Changing the Presence terms URL    | 1 min | ```PresenceVisitor.setTermsUrl(String url)```                                                                                                         | ```Glance.setPresenceTermsUrl(String url)```                                                                                                               |
<br>
*ETA*: 30 minutes
