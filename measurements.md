

# Measurement Examples

|Measurements  |  Images |
:-------------------------:|:-------------------------:
|Base Example |![Base Measurement](./lib/docs_imgs/measurements_0.png) |
|<ul><li>Open Settings Menu View = 170 MB</li><li>.init() = 180MB</li></ul>|![.init](./lib/docs_imgs/measurements_1.png) |
|<ul><li>Open Settings Menu View = 180 MB</li><li>.startSession() = 200MB</li></ul>|![.startSession](./lib/docs_imgs/measurements_2.png) |
|<ul><li>Open Settings Menu View = 165 MB</li><li>.startPresence() = 171MB</li></ul>|![.startPresence](./lib/docs_imgs/measurements_3.png) |
|<ul><li>Open Settings Menu View = 155 MB</li><li>.startPresence() = 170MB</li></ul>|![.startPresence2](./lib/docs_imgs/measurements_4.png) |
|<ul><li>Open Settings Menu View = 153 MB</li><li>.startSession() = 178MB</li></ul>|![.startSession2](./lib/docs_imgs/measurements_5.png) |
|<ul><li>Open Settings Menu View = 150 MB</li><li>.init() = 170MB</li></ul>|![.init2](./lib/docs_imgs/measurements_6.png) |

## Init & Session without Video


| Version | Init App | Glance SDK init (with Presence) | Start session through Presence | Agent Joins Screen Share Session | During session, navigate to a new screen | End session |
:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:
| 6.9.1 | 193MB / 0% | 206MB / 0% | 212MB / 5% | 226MB / 8% | 234MB / 1% | 222MB / 0% |
| 6.8.2 | 163MB / 5% | 150MB / 0% | 184 / 5% | 190MB / 8% | 201MB / 0% | 174MB / 0% |

## Init & Session with Video

| Version | Init App | Glance SDK init (with Presence) | Start session through Presence | Agent Joins Screen Share Session | During session, navigate to a new screen | During session, move video to tab | End session |
:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:|:-------------------------:
| 6.9.1 | 187MB / 0% | 201MB / 0% | 265MB / 8% | 340MB / 14% | 420MB / 18% | 500MB / 10% | 420MB / 3% |
| 6.8.2 | 158MB / 5% | 146MB / 3% | 177 / 8% | 273MB / 18% | 297MB / 20% | 290MB / 14% | 240MB / 5% |
