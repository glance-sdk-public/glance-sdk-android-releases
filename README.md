# Before you start

Our 5.7.16 version includes different (lighter) Proguard rules to avoid compilation issues caused by obfuscated classes/dependencies. If you are not facing any compilation issues with the other versions, just use them.

# Transitioning from 5.7

If you are coming from any version 5.7 and below, please find the instructions of how to transition to the new Glance [here](glance-sdk-transition.md).

# Glance Android SDK

The new mobile Glance SDK allows our customers to use only the basic core features of it. This means that you can implement only the events handling instead of worrying about any UI. To achieve this, please follow the instructions below.

## Setup

### Manually adding the SDK into your project

Follow the instructions on the [helpsite](https://help.glance.net/mobile-app-sharing/getting-started/android/android_quickstart/).

Note: if you face any issues with Multidex, just check if you have enabled it on the app's ```build.gradle``` and also check if you have added the following dependency:

```
implementation "androidx.multidex:multidex:2.0.1"
```

### Using Gradle

The new SDK provides a flexible and easier way to integrate it in your project by using Gradle build tool. So now you just need to follow these two simple steps:

1. On your `settings.gradle`, add our Maven Repository URK:

    ```
    dependencyResolutionManagement {
        ...
        repositories {
            ...
            maven {
                url "https://gitlab.com/api/v4/projects/48670956/packages/maven"
            }
        }
    }
    ```

2. On your app's `build.gradle`, add the following dependency:

    ```
    implementation 'net.glance.android:glance-android-core-sdk:<latest_version>'
    ```

You can check the available versions [here](https://gitlab.com/glance-sdk-public/glance-sdk-android-releases/-/packages).

## Initialization

You must call `Glance.init` once to initialize the visitor class for your group, within an initialization function in your code (`init`, `onCreate`, etc.). Initialize the framework with your Glance Group ID, which is required. The token, name, email and phone fields are optional and are used to identify your user. This information will be stored in Glance session records and available on reports and via our APIs.

You can pass empty strings or any other values useful to you.  The limits are, name: 62 characters, email: 26 characters, phone: 30 characters.

*Java*
```java
Glance.init(activity, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234", "<VISITOR_ID or empty>", listener);
```

*Kotlin*
```kotlin
Glance.init(activity, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234", "<VISITOR_ID or empty>", listener)
```

### Parameters
| Parameter | Type              | Description                                                                                                                                   |
|-----------|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| activity  | `Activity`        | The app's main Activity instance                                                                                                              |
| groupId   | `int`             | The account group ID                                                                                                                          |
| token     | `String`          | User's token                                                                                                                                  |
| name      | `String`          | User's name                                                                                                                                   |
| email     | `String`          | User's email                                                                                                                                  |
| phone     | `String`          | User's phone                                                                                                                                  |
| visitorId | `String`          | User's ID for the Agent to connect with. If you want to initialize the SDK right away with Presence, set a non-empty value for this parameter |
| listener  | `VisitorListener` | The custom VisitorListener implementation to handle events                                                                                    |



## Starting a Session

There are two main options to consider when starting a session: **session key** and **video mode**.

A session can start with or without a specific session key. If you don't want to provide a session key, you need to use the `GLANCE_KEYTYPE_RANDOM` string instead, in order to generate a random one and have it returned in the properties of the `GlanceEvent` with code `EventConnectedToSession`. If you have a known value you wish to use for a session key, such as a customer id, you can pass it to `startSession`.  The key may only contain characters from the Base64URL set, up to 63 characters maximum.

A session can start with multiple different video modes. The options are as follows:
- **VideoOff**: Video for both the agent and user will start OFF
- **VideoSmallVisitor**: Small widget, Video for user will start ON, agent video will start OFF
- **VideoSmallMultiway**: Small widget, Video for user and agent will start ON
- **VideoLargeVisitor**: Large widget, Video for user will start ON, agent video will start OFF
- **VideoLargeMultiway**: Large widget, Video for user and agent will start ON

To start a session with parameters, use the `StartParams` class to set them, and pass that into `startSession`:

*Java*
```java
StartParams startParams = new StartParams();
startParams.setKey(SESSION_KEY); // required
startParams.setVideo(VIDEO_MODE); // optional - defaults to VideoOff
Glance.startSession(startParams);
```

*Kotlin*
```kotlin
val startParams = StartParams()
startParams.setKey(SESSION_KEY) // required
startParams.setVideo(VIDEO_MODE) // optional - defaults to VideoOff
Glance.startSession(startParams)
```

To start a session with a random key call, you must use the "GLANCE_KEYTYPE_RANDOM" string as a key:

*Java*
```java
startParams.setKey("GLANCE_KEYTYPE_RANDOM");
```

*Kotlin*
```kotlin
startParams.setKey("GLANCE_KEYTYPE_RANDOM")
```
## Pausing a Session

A session can be paused while it is running. If a session is paused, then the agent will not be able to view the visitor's screen while the session is paused and will instead receive a blank screen with a message that the session is currently paused. When the session resumes, this prompt will stop showing and the agent will again be able to view the visitor's screen.

The following methods can be used to pause a session, resume a session, and check whether a session is paused or not:

```
Glance.pause(  true||false ) // This will pause the session ( true ), or resume the session ( false ).
 
Glance.togglePause() // This will pause the session if session is active, or unpause the session if session is paused.

Glance.isPaused() // This will return true if the session is paused, false if the session is not currently paused.

```

It is possible to display a custom message for the Agent when pausing a session.
```
Glance.pause(boolean isPaused, String message)
Glance.pause(boolean isPaused, @StringRes int message)
```
Note that message will be truncated if it has more than 1000 characters.

### Note
There are two additional methods, `pauseRecording` and `resumeRecording`. These methods pause/resume the frames (screenshots)
flow being sent to the agent to render the agent view without pausing/resuming the session. These methods are currently used in 
Glance's Flutter bridge implementation, and can be used to pause/resume the frame flow in situations where sensitive information
needs to be hidden that, for some reason, cannot be masked. An example of when these methods might be needed is when integrating 
with Flutter without using Glance's bridge. 
 
## Ending a Session

The session can be ended from either the agent side or the app side.  To end from the app, call:

*Java*
```java
Glance.endSession();
```

*Kotlin*
```kotlin
Glance.endSession()
```

## Visitor Sessions with Media Projection

It is possible to use Android's MediaProjection APIs to capture the screen. Media projection allows the SDK to record the home screen and other apps.

### Enabling Media Projection
If your application targets API 29 or above (Android 10 or above) the following steps need to be taken:

In the `AndroidManifest.xml` file, add the following permissions within the `<manifest>` tag:

```xml
<manifest>
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />
</manifest>
```

and add the following service within the `<application>` tag:

```xml
<service
    android:name="net.glance.android.GlanceMediaProjectionService"
    android:enabled="true"
    android:exported="false"
    android:foregroundServiceType="mediaProjection" />
```

### Listen to Activity Result

When Media Projection is used, the Glance SDK must request the user permission before using Media Projection APIs.
The Glance SDK can request the permission, but given the result is returned via `Activity.onActivityResult()`, the SDK can't capture the result by itself.
Therefore, the host app needs to capture and share the permission result with the Glance SDK.

```java
public class MyActivity implements VisitorListener {

    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Glance SDK request the user permissions using 'HostSession.REQUEST_SCREEN_SHARE' as requestCode.
        if (requestCode == HostSession.REQUEST_SCREEN_SHARE && Glance.isInSession()) {
            if (resultCode == Activity.RESULT_OK) {
                // Share the 'data' Intent with the CoreSDK
                Glance.onCaptureScreenPermissionSuccess(data);
            } else {
               Glance.onCaptureScreenPermissionFailure();
            }
        }
    }
}
```

### Start a Visitor Session

To capture only the current app, excluding other apps or the home screen, ensure that `StartParams` is configured as follows:

```java
startParams.setMediaProjectionEnabled(true);
```

To capture the entire device—including the app, other apps, and the home screen—configure the following settings instead:

```java
startParams.setCaptureEntireScreen(true);
```

The Glance SDK is unable to mask elements or sensitive information outside the app in which it is installed. So before enabling `setCaptureEntireScreen`, 
 consider the security implications of information being exposed by other apps or on the home screen.

### Permissions to enable Media Projection

The SDK will request full screen capture permission at runtime on devices running Android Lollipop or later if `setCaptureEntireScreen` is set to true (as seen above). If the user accepts, the session will utilize the permission to record the full screen.  If the user rejects, the SDK falls back to the solution that is used by pre-Lollipop Android devices to record the screen.

#### Masking and Gestures Outside of the Application
Capturing the full screen on Android allows the SDK to record the home screen and other apps. When the user is outside the context of the app, the SDK cannot mask views or display gestures to the user.  This means that keyboards and input fields outside of the app integrating the SDK will be fully visible to the agent which may present security issues. Consider your app use case carefully before enabling Media Projection.

#### Masking the Keyboard
Keyboard masking allows the SDK to cover the keyboard when it's in use. This prevents sensitive user information from being communicated to the agent. This feature only applies when permission to capture the entire screen is enabled and approved by the user.  Otherwise, keyboard masking is not necessary because the keyboard is not captured as part of the screenshare.

As a result of keyboard masking being specific to Media Projection, it should be noted that keyboards displayed outside of the app integrating the SDK (other apps on the device and home screen) will not be masked.

To enable keyboard masking:

```
Glance.maskKeyboard(true);
```

### IMPORTANT

As part of the Media Projection implementation, the Glance SDK uses the `android.permission.FOREGROUND_SERVICE_MEDIA_PROJECTION` permission.
If your app does not use Media Projection, remove that permission (especially if your app is targeting the API 34).
Add the following line to your app's `AndroidManifest.xml` file:

```xml
<uses-permission android:name="android.permission.FOREGROUND_SERVICE_MEDIA_PROJECTION" tools:node="remove" />
```
Apps targeting API 34 may need to add extra information on their Google Play store listening page if the permission above is found on their Manifest file.
Therefore, if your app does not use media projection, it is recommended to remove that permission before uploading your app to Google Play Console.

## Masking

### Masking views

If you have sensitive information in your app that should be masked during guided customer experiences, you can notify our SDK and it will redact those views from its screen capture.

| Method                                           | Description                                                                         |
|--------------------------------------------------|-------------------------------------------------------------------------------------|
| `void addMaskedViewId(int viewId, String label)` | Adds a view ID and a label on the SDK's masking manager's views-to-mask list.       |
| `void addMaskedView(View view, String label)`    | Adds a view instance and a label on the SDK's masking manager's views-to-mask list. |
| `void removeMaskedViewId(int viewId)`            | Removes a view ID from the SDK's masking manager's views-to-mask list.              |
| `void removeMaskedView(View view)`               | Removes a view instance from the SDK's masking manager's views-to-mask list.        |

---

### Masking Secure Screens

You can also masked secure windows (screens with `WindowManager.LayoutParams.FLAG_SECURE` flag).

```
void setUseAndroidFlagSecureMasking(Boolean maskSecureDisplay, @Nullable String label)
```

This option is disabled, by default.

By passing `true` as the first parameter, you enable the SDK to mask secure screens. You can also specify the optional `label` (it can be null) to render some text over the redacted screen.

### Masking with Jetpack Compose

It is also possible to mask composable screens. Glance has provided the following modifier for composable functions

```kotlin
Modifier.glanceMask(label: String, listState: ScrollableState? = null)
```

You can specify two parameters when masking a view in compose:
 - label: Text that will be visible for the Agent
 - listState (optional): You can pass a ScrollableState in order to pause the streaming while user is scrolling

Example:

```kotlin
Text(
    modifier = Modifier.glanceMask("Redacted"),
    text = "Sensitive Information here"
)

val lazyListState = rememberLazyListState()
LazyColumn(
    state = lazyListState,
) {
    items(20) {
        Text(
            modifier = Modifier.glanceMask("Redacted", lazyListState),
            text = "Sensitive Information here"
        )
    }
}
```

### Inside a WebView
To support masking inside a WebView, you will need to make some changes to the way your WebView is setup.
Both `GlanceWebViewJavascriptInterface` and `GlanceWebViewClient` need to be added to your WebView:

    var webViewClient: GlanceWebViewClient? = null
    var jsInterface: GlanceWebViewJavascriptInterface? = null

    // webView below would be your WebViewActivity class:
    webView.settings.javaScriptEnabled = true
    webView.settings.domStorageEnabled = true
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        webView.settings.mixedContentMode = 2
    }

    jsInterface = GlanceWebViewJavascriptInterface(webView)
    webView.addJavascriptInterface(jsInterface, "GLANCE_Mask")

    String querySelectors = "SELECTOR1,SELECTOR2,SELECTOR3,...";
    String labels = "LABEL1,LABEL2,LABEL3,..."
    webViewClient = GlanceWebViewClient(querySelectors, labels)
    webView.webViewClient = webViewClient as GlanceWebViewClient
    webView.loadUrl(maskingUrl!!)


If launching the WebView as a separate activity, you can pass your `querySelectors` and `labels` strings to mask via the Intent mechanism:

    val i = Intent(applicationContext, WebViewActivity::class.java)
    i.putExtra("url", maskingUrl)
    i.putExtra("querySelectors", querySelectors)
    i.putExtra("labels", labels)
    startActivity(i)


In the `onDestroy` method of your WebView, make sure to call the following to ensure proper cleanup of WebView masks:

    webView!!.removeJavascriptInterface("GLANCE_Mask")
    jsInterface!!.onDestroy()


For the list of query selectors, you can pass in anything that will be found via the `document.querySelectorAll()` javascript method. When the WebView is loaded, we'll search for the given query selectors, find it's location on the screen, apply a mask over the entire element, and apply a label on top of the mask, if provided.
The following selectors will be masked by default:

Any element with the class <b>glance_masked</b>

Any element with the attribute <b>glance_masked="true"</b>

Any text input element with <b>type="password"</b>


On the agent side, the WebView will be paused until it is fully loaded and the masks have been added. The WebView will also pause when the user is scrolling within the WebView, allowing the masks to be repositioned without the agent seeing any of the content underneath.
This implementation also supports webpage mutations and masking within iframes.

Custom WebViewClient
If your WebView implementation already has a custom WebViewClient then you have two options: subclass GlanceWebViewClient or manage your own instance of GlanceWebViewClient.
If you choose to subclass, ensure calls to super are made within the onPageStarted and onPageFinished methods.
If you choose to create your own instance of GlanceWebViewClient, ensure calls to onPageStarted and onPageFinished are forwarded to the GlanceWebViewClient that you are maintaining.


### Masking and Media Projection

It is important to mention that the SDK may not be able to mask views at all times if you are using Android's media projection as the capture mode.

Users can choose to either share only the app or their full screen when using media projection. However, when the full screen is selected, the entirety of their screen is shared all the time, and the SDK won't be able to detect the position of the views properly when the user is using features like window split or when the user is viewing the list of recent apps.


## Presence

The Glance SDK can automatically handle a presence connection by adding the following:

*Java*

```java
public class MyActivity implements VisitorListener {

    //...

    public void onGlanceVisitorEvent(Event event) {

       //...

       // PRESENCE CODES
        EventCode eventCode = event.getCode();
        if (eventCode == EventCode.EventVisitorInitialized) {
            // session initialized
            Glance.connectToPresence();
            Glance.setPresenceTermsUrl("https://github.com/"); // OPTIONAL
        } else if (eventCode == EventCode.EventPresenceConnected) {
            // ready to start a presence session
            startParams.setKey(/*<YOUR_VISITOR_ID>*/);
            Glance.startSession(startParams, true);
        } else if (eventCode == EventCode.EventPresenceDisconnected) {
            // Presence has been disconnected    
        } else if (eventCode == EventCode.EventPresenceConnectFail) {
            Log.d(TAG, "Received event - " + eventCode);
        } else if (eventCode == EventCode.EventPresenceShowTerms) {
            Log.d(TAG, "Received event - " + eventCode);
        } else if (eventCode == EventCode.EventPresenceSignal) {
            Log.d(TAG, "Received event - " + eventCode);
        } else if (eventCode == EventCode.EventPresenceBlur) {
            Log.d(TAG, "Received event - " + eventCode);
        }
    }
}
```
*Kotlin*

```kotlin
class MyActivity : VisitorListener {

    //...

    override fun onGlanceVisitorEvent(event: Event) {
        //...

        // PRESENCE CODES
        when (val eventCode = event.code) {
            EventCode.EventVisitorInitialized -> {
                // session initialized
            }
            EventCode.EventPresenceConnected -> {
                // ready to start a presence session
                startParams.setKey(/*< YOUR_VISITOR_ID >*/)
                Glance.startSession(startParams, true)
            }
            EventCode.EventPresenceDisconnected -> {
                // Presence has been disconnected    
            }
            EventCode.EventPresenceConnectFail -> {
                Log.d(TAG, "Received event - $eventCode")
            }
            EventCode.EventPresenceShowTerms -> {
                Log.d(TAG, "Received event - $eventCode")
            }
            EventCode.EventPresenceSignal -> {
                Log.d(TAG, "Received event - $eventCode")
            }
            EventCode.EventPresenceBlur -> {
                Log.d(TAG, "Received event - $eventCode")
            }
        }
    }
}
```

NOTE:
You must wait for <code>EventVisitorInitialized</code> before calling <code>startSession</code>.

## Miscellaneous

### Max Attempts

It is possible to set the maximum number of attempts that the SDK can make while trying to perform some HTTP calls. Below you can find which features supports it and what happens when the counter reaches the maximum value.

In order to set the number of attempts, you can use `GlanceAttempts(int maxAttempts)` object. `maxAttempts` should be positive and greater than zero. Otherwise, it will be ignored.

##### Glance.init(...., GlanceAttempts(int maxAttempts))

It is possible to set the number of attempts for the initialization method by providing a reference to a `GlanceAttempts` object. This will limit the number of API calls that can be performed during the initialization. The result of those API calls does not impact the result of the SDK initialization. This way, after reaching the maximum number of attempts, `EventVisitorInitialized` event is produced.

##### Glance.startSession(...., GlanceAttempts(int maxAttempts))

It is possible to set the maximum number of attempts that can be made while starting a session. If the session couldn't be established after reaching that value, `EventStartSessionFailed` event is produced.

##### Glance.connectToPresence(...., GlanceAttempts(int maxAttempts))

You can control how many attempts can be made when connecting or reconnecting to the presence server. After the maximum value is reached, the connection will be paused and the `EventPresenceBlur` event will be produced.

Since the connection is paused, the SDK will try to resume the connection to presence server whenever the apps is foregrounded again.

### Timeouts

It is possible to set custom timeouts for specific methods by passing a `GlanceTimeout(long timeoutInMillis)` object. `timeoutInMillis` should be positive and greater than zero. Otherwise, it will be ignored. Below is the list of methods that allow custom timeouts:

##### Glance.init(...., GlanceTimeout(initTimeoutMillis))

Timeout in MILLISECONDS for API calls that can be performed during the initialization. The SDK will be initialized even if the API call fails.

##### Glance.startSession(...., GlanceTimeout(initTimeoutMillis))

Timeout in MILLISECONDS specifying how much time the Visitor should wait for the agent to connect.

The agent cannot join the session due to errors on the agent side or because the device is not
able to start the session (due to network errors, for example).
If the agent does not join the session until that timer expires, the session will end and `EventSessionEnded` event will be produced.

If the timeout expires because of network errors preventing the session from being started, you can set the timeout as follows:
```Java
Glance.startSession(/*.... */, new GlanceTimeout(initTimeoutMillis, GlanceTimeout.Type.Network))
```

##### Glance.connectToPresence(...., GlanceTimeout(initTimeoutMillis))

Timeout in MILLISECONDS to connect (or reconnect) to the presence servers. After that time is reached, `EventPresenceDisconnected` event will be produced and presence will be disconnected (not paused or blurred, but disconnected).


### Sending a message to the Agent:

*Java*
```java
Map<String, String> paramsMap = new HashMap<>();
paramsMap.put("url", "webview " + url);
Glance.sendToPresenceSession("presence", paramsMap);
```

*Kotlin*
```kotlin
val paramsMap: MutableMap<String, String> = HashMap()
paramsMap["url"] = "webview $url"
Glance.sendToPresenceSession("presence", paramsMap)
```

### Disconnecting from Presence

*Java*
```java
Glance.disconnectPresence();
```

*Kotlin*
```kotlin
Glance.disconnectPresence()
```

### Pausing and Resuming presence

```kotlin
// Pause
Glance.pausePresence(true)

// Resume
Glance.pausePresence(false)
```

## Events

Before starting a session you should register a VisitorListener listener to monitor session events:

*Java*
```java
public class MyActivity implements VisitorListener {

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Glance.init(this, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234", "", this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Glance.removeVisitorListener(this);
    }

    // this is called on the Glance Event thread
    public void onGlanceVisitorEvent(Event event) {
        EventCode eventCode = event.getCode();
        if (eventCode == EventCode.EventConnectedToSession){
            // Show any UI to indicate the session has started
            // If not using a known session key, you can get the random key here
            // to display to the user to read to the agent
            final String sessionKey = event.GetValue("sessionkey");
        }
        else if (eventCode == EventCode.EventGuestCountChange) {
            // to receive this event, an agent must join the session
        }
        else if (eventCode == EventCode.EventChildSessionStarted) {
            // to receive this event, the visitor must accept to add his/her video
        }
        else if (eventCode == EventCode.EventChildSessionEnded) {
            // to receive this event, the visitor must remove his/her video
        }
        else if (eventCode == EventCode.EventSessionEnded ){
            // dismiss any UI or reset variables
        }
        else if (event.getType() == EventType.EventWarning ||
                 event.getType() == EventType.EventError ||
                 event.getType() == EventType.EventAssertFail) {
            // Best practice is to log code and message of all events of these types
        }
    }
}
```

*Kotlin*
```kotlin
class MyActivity : VisitorListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Glance.init(this, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234", this)
    }

    override fun onDestroy() {
        super.onDestroy()

        Glance.removeVisitorListener(this)
    }

    // this is called on the Glance Event thread
    override fun onGlanceVisitorEvent(event: Event) {
        when(event.getCode()) {
            EventCode.EventConnectedToSession -> {
                // Show any UI to indicate the session has started
                // If not using a known session key, you can get the random key here
                // to display to the user to read to the agent
                val sessionKey = event.GetValue("sessionkey")
            }
            EventCode.EventGuestCountChange -> {
                // to receive this event, an agent must join the session
            }
            EventCode.EventChildSessionStarted -> {
                // to receive this event, the visitor must accept to add his/her video
            }
            EventCode.EventChildSessionEnded -> {
                // to receive this event, the visitor must remove his/her video
            }
            EventCode.EventSessionEnded -> {
                // dismiss any UI or reset variables
            }
            else -> {
                if (event.type == EventType.EventWarning ||
                    event.type == EventType.EventError ||
                    event.type == EventType.EventAssertFail
                ) {
                    // Best practice is to log code and message of all events of these types
                }
            }
        }
    }
}
```

NOTE:
You must wait for <code>EventVisitorInitialized</code> before calling <code>startSession</code>.

# Integrating the two-way-video

Please follow the steps described [here](https://gitlab.com/glance-sdk-public/glance-default-ui-android-releases/-/tree/main#video-integration).


# Agent Info and Dynamic Masking

## Agent Information

Agent (and other guest) information will be passed to your application on the `EventGuestCountChanged` event.  You can handle this event in your event handler function.

The list of guest information is a JSON array on the `"guestlist"` property of the event.  For example:

```json
[
    {
        "role": "visitor"
    },
    {
        "agentrole": "Supervisor",
        "name": "Dr. Rich Baker",
        "partnerid": 12345,
        "partneruserid": "rbaker",
        "role": "agent",
        "title": "Gentleman + Scholar",
        "username": "rich.glance.net"
    }
]
```

Note that `"role"` is the participant role (one of `"visitor"`, `"agent"`, `"guest"`, `"host"`), while `"agentrole"` is the name of the Role (if any) set up for Role-based permissions.

By default accounts are configured to send only the `"role"` and the `"agentrole"` for agents, but sending of other agent information such as name, username and partneruserid can be enabled.

The agent information passed on `guestlist` can be useful for implementing masking based on agent role, username, or partneruserid.

## Role-based Masking

The agent information passed on `guestlist` can be useful for implementing masking based on agent role, username, or partneruserid.
You will need to handle any masking, pausing of screenshare, or any customization of your application you wish to do based on agents or agent roles.

This code from the sample will pause screensharing whenever any agent joins with a role named "pause".  Otherwise it will mask two fields if any agent with role "mask" joins.  If only agents in other roles join, there will be neither pausing nor masking:

```java
private void handleEvent(Event event) {
    if (event.getCode() == EventCode.EventGuestCountChange) {
        try {
            // Get the guest list
            JSONArray guestList = new JSONArray(event.GetValue("guestlist"));
            for (int guests = 0; guests < guestList.length(); guests++) {
                JSONObject guest = guestList.getJSONObject(guests);
                // Look for the agent
                if ("agent".equals(guest.get("role"))) {
                    // Get the agent role
                    switch ((String) guest.get("agentrole")) {
                        case "pause":
                            // Pause what needs to be paused
                            break;
                        case "mask":
                            // Mask what needs to be masked
                            break;
                        default:
                            System.out.println("agent role is missing or ignored");
                    }
                    break;
                }
            }
        } catch (JSONException | NullPointerException exception) {
            exception.printStackTrace();
        }
    }
}
```

## Starting Paused

When a screenshare session starts, the contents of the screen are being streamed to the Glance service even before any agent joins.  If an agent with a limited role joins they may briefly see inappropriate information before the masking or pausing above is applied.

In order to prevent that the session should be started in a paused state.  Then when an agent joins the correct masking can be applied before the session is "unpaused".

To start a session in a paused state, call the `Glance.startSession` method with `StartParams` like this:

```java
StartParams startParams = new StartParams();
startParams.setKey(sessionKey);
startParams.setPaused(true);
Glance.startSession(startParams);
```

## Multiple Roles

If an agent has joined and screensharing is enabled (not paused) and an additional agent were to join with a more limited role, this agent could briefly see inappropriate information.  To prevent this situation your Glance account can be configured to disallow additional agents with different roles from joining a session.


## Responding to Agent Events

During an active session, the agent has a series of controls which allows them to move, collapse, and expand the widget shown on the client’s screen as well as start video.

The SDK provides methods which are invoked when these events are received from the server (triggered by Agent’s actions). These methods can update the UI to respond accordingly, but are not required.

For example, if video is not desired to be displayed, even if the agent tries to launch video, the app should not respond to the video delegate methods that are triggered and not take any actions in updating the UI.

### Agent Portal Controls

When an agent is in a session they have the option to turn their camera on or off which will start video as well as the ability to toggle the widget size and location displayed in the visitor's view.

Toggling the widget from Full to Tab will collapse the tab on the device, and tapping in any of the corners will move the widget to one of the corners of the screen. This is the behavior when using the DefaultUI, but as mentioned earlier, these events from the server can be implemented (or not) by any custom UI implementation.

### Video Delegate Methods

The following methods are used when displaying agent video and agent video requests.

Agent and video events in Android are handled through your implementation of the SessionUI class with the following methods being invoked when the agent takes the corresponding action.

#### void startVisitorVideo(StartParams sparams, int groupId, boolean invokeShowWidget);

Invoked when the visitor video is ready to be added. Here you can set up the VideoSession and then show the visitor video preview in the UI.

#### void onAgentVideoConnected(SessionUICompletionListener listener);
Called when the agent video is ready to be shown to the visitor. Here you need to call GlanceManager.getInstance().setCustomSessionViewId(<your net.glance.android.SessionView id>); to link the SessionView from your layout with the video stream.

#### void changeAgentVideoState(boolean isAgentVideoEnabled);
Called when the agent video state changes. You can use it to decide whether to show or hide some UI that handles the agent video on the visitor side.

#### boolean isVisitorVideoStreaming();
Called when the SDK is ready to show the visitor video streaming. We need to check this status in order to decide whether to add the video or not (in case it is already added). You can use your VideoSession instance to verify it.

#### void promptVisitorVideo(VideoMode videoMode);
Triggered when the event EventVisitorVideoRequested is received, the terms are not accepted yet, and video mode is not set to VideoMode.VideoOff

### Widget Location & Appearance Agent Requests

The following methods are used in connection with agent requests that can be made to change the location of the widget or to collapse and expand the widget appearance.

#### public void processUserMessages( Event event )

Widget Location and Appearance methods are passed to the app through this method as separate events. The events are listed below and should be handled separately, for example in a switch statement.


`EventConstants.ATTR_MESSAGE_WIDGET_LOCATION`

This event is sent when the widget location is updated by the agent.

`EventConstants.ATTR_MESSAGE_WIDGET_VISIBILITY`

This event is sent when the widget is toggled between Full and Tab.


# Performance metrics

Below you can find are our current measurements for the latest 6.X SDK version. Notes:

- We used the DefaultUI app for both builds, which is a simple app with only default settings for everything (dependencies, Proguard rules, etc);
- For the UI-less mode, we used another app that is even simpler;
- We executed the measurement considering both .apk and .aar formats for the app;
- This is a simple app with only default settings for everything (dependencies, Proguard rules, etc).

|      Version     |           .apk Compilation time           |            .aar Compilation time          |   SDK size  |
|------------------|-------------------------------------------|-------------------------------------------|-------------|
| 6.9.1.0          | 15s (first time), 1s (second time and on) | 29s (first time), 1s (second time and on) |      5M     |
| 6.1.5 (with UI)  | 42s (first time), 2s (second time and on) | 13s (first time), 2s (second time and on) |      7MB    |
| 6.1.5            | 30s (first time), 2s (second time and on) | 5s (first time), 2s (second time and on)  |    5.01MB   |
| 5.7.15.2         | 40s (first time), 5s (second time and on) | 12s (first time), 2s (second time and on) |     5.4M    |

# Webserver Codes

The Glance webserver may return the following messages or error codes to the SDK during usage. You may see them in logging while integrating the SDK.

## Connection Messages
| Name              | Value                 | Built-in Descriptions                                                                                                                                          | Explanation                                                |
|-------------------|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------|
| OK                | SWAP16(0x0000)        | -                                                                                                                                                              | Connection is established successfully                     |
| FAIL              | SWAP16(0x1000)        | "Invalid session id"	<br> "Invalid client info" <br> "Duplicate display connect" <br> "Invalid protocol verion for continuous updates"                         | Connection failed because of invalid data.                 |
| FAIL_VERSION      | FAIL + SWAP16(0x0001) | -                                                                                                                                                              | Connection falied because versions are incompatible.       |
| FAIL_UNAUTHORIZED | FAIL + SWAP16(0x0002) | "Not authorized to join this session"	<br> "Start screenshare not allowed"<br>"Unauthorized"<br>"Not authorized to join this session"<br>"Server key required" | Connection failed because of authorization problems.       |
| FAIL_NO_SESSION   | FAIL + SWAP16(0x0003) | "The session has ended"                                                                                                                                        | Connection failed because the requested session has ended. |
| FAIL_SSN_FULL     | FAIL + SWAP16(0x0005) | -                                                                                                                                                              | Session serial number problem.                             |

## Close Messages
| Name          | Value          | Description                                 |
|---------------|----------------|---------------------------------------------|
| QUIT          | SWAP16(0x0000) | Session ended.                              |
| TIMED_OUT     | SWAP16(0x0001) | Timed out waiting for the other side.       |
| SERVER_ERROR  | SWAP16(0x0002) | Server is closing because it errored.       |
| CLIENT_ERROR  | SWAP16(0x0003) | Client is closing because it errored.       |
| PEER_ERROR    | SWAP16(0x0004) | Other side closed because it errored.       |
| RESET         | SWAP16(0x0005) | Client sent something invalid.              |
| RECONNECT     | SWAP16(0x0006) | Client should re-connect to another server. |
| SERVER_KILLED | SWAP16(0x0007) | Server killed session intentionally.        |
| DECLINED      | SWAP16(0x0008) | Guest declined to share their screen.       |
| CONN_DEAD     | SWAP16(0x0009) | Connection timed out due to inactivity.     |
| CALL_DEAD     | SWAP16(0x000A) | Call timed out due to inactivity.           |
| REPLACED      | SWAP16(0x000B) | A guest opened a second viewer.             |
| INFO_SHUTDOWN | SWAP32(0x0001) | Closing due to system shutdown or logoff.  |
